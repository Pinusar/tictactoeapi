package com.example.tictactoeapi.engine;

import java.util.HashMap;
import java.util.Map;

public class Board {

    private HashMap<Integer, String> state;

    public Board() {
        this.state = state = new HashMap<>();
        state.put(1, ".");
        state.put(2, ".");
        state.put(3, ".");
        state.put(4, ".");
        state.put(5, ".");
        state.put(6, ".");
        state.put(7, ".");
        state.put(8, ".");
        state.put(9, ".");
    }

    public Board(HashMap<Integer, String> state) {
        this.state = state;
    }

    public HashMap<Integer, String> getState() {
        return state;
    }

    public void setState(HashMap<Integer, String> state) {
        this.state = state;
    }

    public String getStateAsString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry entry: this.state.entrySet()
             ) {
            result.append(entry.getValue());
        }
        return result.toString();
    }

    public void setStateFromString(String stateString) {
        if (stateString.length() != 9) {
            throw new IllegalArgumentException("Bad state string provided! Length must be 9.");
        }
        HashMap<Integer, String> state = new HashMap<>();
        var chars = stateString.toCharArray();
        for (int i = 1; i < 10; i++) {
            String mark = String.valueOf(chars[i - 1]);
            if (!mark.equals(".") && !mark.equals("X") && !mark.equals("O")) {
                throw new IllegalArgumentException("Bad state string provided! State string should only contain [.XO].");
            }
            state.put(i, mark);
        }
        this.setState(state);
    }
    
    public String displayAsText() {
        return String.format("+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+\n" +
                "| %s | %s | %s |\n" +
                "+---+---+---+", state.get(1), state.get(2), state.get(3), state.get(4),
                state.get(5), state.get(6), state.get(7), state.get(8), state.get(9));
    }
}
