package com.example.tictactoeapi.controller;

import com.example.tictactoeapi.model.Move;
import com.example.tictactoeapi.service.TicTacToeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TicTacToeController {

    private TicTacToeService service;

    public TicTacToeController(TicTacToeService service) {
        this.service = service;
    }

    @GetMapping("/play")
    public Move playMove(@RequestParam String boardState) {
        return service.findMove(boardState);
    }
}
