package com.example.tictactoeapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Move {
    private Integer targetTile;
    private String boardState;
}
