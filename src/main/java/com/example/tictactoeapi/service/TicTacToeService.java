package com.example.tictactoeapi.service;

import com.example.tictactoeapi.engine.Board;
import com.example.tictactoeapi.engine.BoardEvaluator;
import com.example.tictactoeapi.engine.GameNode;
import com.example.tictactoeapi.model.Move;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class TicTacToeService {

    public Move findMove(String boardState) {
        Board board = new Board(new HashMap<>());
        board.setStateFromString(boardState);
        BoardEvaluator evaluator = new BoardEvaluator("O");
        GameNode root = new GameNode();
        root.setBoard(board);
        int bestMove = evaluator.findNextMove(root, "O").getTargetTile();
        board.getState().put(bestMove, "O");
        String newState = board.getStateAsString();
        return new Move(bestMove, newState);
    }
}
