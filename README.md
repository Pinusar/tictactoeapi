**Tic-tac-toe REST API**

A REST api for playing tic-tac-toe using Spring Boot.

Java 11 is required for running it.

To start the application clone the repository and run via Gradle.

`git clone https://gitlab.com/Pinusar/tictactoeapi.git`

`cd tictactoeapi`

`./gradlew bootRun`

By default, the app will run on localhost:8080

To play, send a GET request to the following endpoint: localhost:8080/play

Add a query parameter _boardState_ representing the current board state. Example: X..O..X.. 

Sample query: _http://localhost:8080/play?boardState=XXO..O.X._

Sample response:
`{"targetTile":5, "boardState":"XXO.OO.X."}`

targetTile - the tile where computer makes the next move.

boardState - the new board state after computer's move.